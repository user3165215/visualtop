#include "sshconnecter.h"
#include <stdio.h>

SSHConnecter::SSHConnecter()
{
    session = NULL;
    channel = NULL;
    connected = false;
}
SSHConnecter::~SSHConnecter() {
    disconnect();
    delete channel;
    delete session;
}

int SSHConnecter::connect(const char *hostname, int port, const char *username, const char *password) {
#ifdef WIN32
    WSADATA wsadata;

    WSAStartup(MAKEWORD(2,0), &wsadata);
#endif
    int rc = libssh2_init (0);
    if (rc != 0) {
       fprintf (stderr, "libssh2 initialization failed (%d)\n", rc);
       return -1;
    }

    int i = 0;
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = inet_addr(hostname);

    if (::connect(sock, (struct sockaddr*)(&sin),
                    sizeof(struct sockaddr_in)) != 0) {
            fprintf(stderr, "Failed to connect!\n");
            return -2;
    }

    session = libssh2_session_init();
    if (libssh2_session_handshake(session, sock)) {
            fprintf(stderr, "Failure establishing SSH session\n");
            return -3;
    }
    const char *fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1);
    printf("Fingerprint: ");
    for(i = 0; i < 20; i++) {
        printf("%02X ", (unsigned char)fingerprint[i]);
    }
    printf("\n");

    /* check what authentication methods are available */
    char *userauthlist = libssh2_userauth_list(session, username, strlen(username));
    printf("Authentication methods: %s\n", userauthlist);
    if (strstr(userauthlist, "password") == NULL) {
        fprintf(stderr, "Failure establishing SSH session\n");
        disconnect();
        return -4;
    }

   /* We could authenticate via password */
    if (libssh2_userauth_password(session, username, password)) {
       printf("\tAuthentication by password failed!\n");
       disconnect();
       return -5;
   } else {
       printf("\tAuthentication by password succeeded.\n");
    }

    /* Request a shell */
    if (!(channel = libssh2_channel_open_session(session))) {
        fprintf(stderr, "Unable to open a session\n");
        disconnect();
        return -1;
    }
    connected = true;

    return 0;

}

int SSHConnecter::disconnect() {
    connected = false;
    if (channel != NULL) {
        channel = NULL;
    }
    if (session != NULL) {
        libssh2_session_disconnect(session, "Normal Shutdown, Thank you for playing");
        libssh2_session_free(session);
    }
    return 0;
}

int SSHConnecter::exec(const char *command, QString *result) {
    int rc;
    char buffer[256];
    unsigned int nbytes;

    if (!(channel = libssh2_channel_open_session(session))) {
        fprintf(stderr, "Unable to open a session\n");
        disconnect();
        return -1;
    }

    rc = libssh2_channel_exec(channel, command);
    if (rc != 0)
    {
        return rc;
    }

    nbytes = libssh2_channel_read(channel, buffer, sizeof(buffer));
    while (nbytes > 0)
    {
        if (write(1, buffer, nbytes) != nbytes)
        {
            return -1;
        }
        result->append(buffer);
        nbytes =libssh2_channel_read(channel, buffer, sizeof(buffer));
    }

    if (nbytes < 0)
    {
        return -1;
    }

    libssh2_channel_send_eof(channel);
    libssh2_channel_free(channel);

    return 0;
}

bool SSHConnecter::isConnected() {
    return connected;
}

