#include "processestab.h"
#include "ui_processestab.h"
#include <stdio.h>

ProcessesTab::ProcessesTab(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProcessesTab)
{
    ui->setupUi(this);
    QList<Process> processes;

    QStringList *columns = new QStringList();
    columns->append(Process::PID);
    columns->append(Process::PPID);
    columns->append(Process::UID);
    columns->append(Process::USER);
    columns->append(Process::PR);
    columns->append(Process::CPU);
    columns->append(Process::MEM);
    columns->append(Process::TIME);
    columns->append(Process::COMMAND);
    setTableWidget(ui->tableWidget, columns, &processes);
}

int ProcessesTab::updateData(SSHConnecter *con) {
    QList<Process> processes;
    QString result("");
    con->exec("ps -eo pid,uid,user:20,pri,%cpu,%mem,time,comm O -C", &result);
    QStringList rows = result.split("\n");
    QStringList columns = Process::parseColumns(rows.at(0));
    for (int i = 1; i < rows.size(); ++i) {
         processes.append(Process(columns, rows.at(i)));
    }
    setTableWidget(ui->tableWidget, &columns, &processes);
}

void ProcessesTab::setTableWidget(QTableWidget *tableWidget, QStringList *columns, QList<Process> *processes) {
    tableWidget->clear();
    tableWidget->setSortingEnabled(false);
    tableWidget->setColumnCount(columns->size());
    tableWidget->setRowCount(processes->size() - 1);
    tableWidget->setHorizontalHeaderLabels(*columns);
    for (int i = 0; i < processes->length(); ++i) {
        setRow(tableWidget, processes->at(i), i);
    }
    tableWidget->setSortingEnabled(true);
}

void ProcessesTab::setRow(QTableWidget *tableWidget, Process process, int row) {
    QStringList values = process.getValues();
    for (int i = 0; i < values.size(); ++i) {
        QTableWidgetItem *item = process.getWidget(i);
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        tableWidget->setItem(row, i, item);
    }
}

ProcessesTab::~ProcessesTab()
{
    delete ui;
}
