#ifndef CONNECTDIALOG_H
#define CONNECTDIALOG_H

#include <QDialog>
#include "sshconnecter.h"

namespace Ui {
class ConnectDialog;
}

class ConnectDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ConnectDialog(SSHConnecter *con, QWidget *parent = 0);
    ~ConnectDialog();
private slots:
    void sshConnect();
    void cancel();
private:
    void showError(int result);
    Ui::ConnectDialog *ui;
    SSHConnecter *con;

};

#endif // CONNECTDIALOG_H
