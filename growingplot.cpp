#include "growingplot.h"

GrowingPlot::GrowingPlot(QWidget *parent) :
    QwtPlot(parent)
{
}

GrowingPlot::GrowingPlot(int n)
{
    current_size = 0;
    size = n;
    QPen pen = QPen(Qt::red);
    QBrush brush = QBrush(Qt::blue);

    curve = new QwtPlotCurve;
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setPen(pen);
    curve->setBrush(brush);
    curve->attach(this);
}

void GrowingPlot::Append(double x_value, double y_value)
{
    if (current_size < size) {
        this->x.push_back(x_value);
        this->y.push_back(y_value);
        current_size++;
    } else {
        this->x.pop_front();
        this->x.push_back(x_value);
        this->y.pop_front();
        this->y.push_back(y_value);
    }
    curve->setData(this->x, this->y);
    replot();
}
