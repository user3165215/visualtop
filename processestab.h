#ifndef PROCESSESTAB_H
#define PROCESSESTAB_H

#include <QWidget>
#include <QTableWidget>
#include "process.h"
#include "sshconnecter.h"

namespace Ui {
class ProcessesTab;
}

class ProcessesTab : public QWidget
{
    Q_OBJECT
    
public:
    explicit ProcessesTab(QWidget *parent = 0);
    ~ProcessesTab();
    int updateData(SSHConnecter *con);
    
private:
    Ui::ProcessesTab *ui;
    void setTableWidget(QTableWidget *tableWidget, QStringList *columns, QList<Process> *processes);
    void setRow(QTableWidget *tableWidget, Process process, int row);
};

#endif // PROCESSESTAB_H
