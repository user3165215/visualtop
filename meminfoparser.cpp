#include "meminfoparser.h"
#include <QStringList>

MemInfoParser::MemInfoParser(QString str)
{
    QStringList lines = str.split("\n");
    QString line = lines[1];
    QStringList values = line.split(QRegExp("[\\s]+"));
    memInfo = new MemInfo(values[1].toLongLong(), values[2].toLongLong());
}

MemInfo *MemInfoParser::GetMemInfo()
{
    return memInfo;
}
