#include "logicalprocessorswidget.h"
#include "ui_logicalprocessorswidget.h"
#include <stdio.h>
#include <QPainter>

LogicalProcessorsWidget::LogicalProcessorsWidget(int percent, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogicalProcessorsWidget)
{
    this->percent = percent;
    ui->setupUi(this);
    setBackground();

}

LogicalProcessorsWidget::~LogicalProcessorsWidget()
{
    delete ui;
}

void LogicalProcessorsWidget::update(int percent)
{
    this->percent = percent;
    QString str = QString::number(percent);
    str.append("%");
    ui->percentLabel->setText(str);
    repaint();
}

void LogicalProcessorsWidget::setBackground()
{
    this->setGeometry(0,0,300,100);
    QPalette Pal(palette());
    Pal.setColor(QPalette::Background, QColor(0, 0, 255));
    this->setAutoFillBackground(true);
    this->setPalette(Pal);
}

void LogicalProcessorsWidget::paintEvent(QPaintEvent *)
{
//    QPainter p;
//    p.begin( this );
//    p.setPen(QColor(0, 0, 0));
//    p.drawText(0, 0, "Text");
//    p.end();
    QPalette Pal(palette());
    printf("\nPercent in paintEvent!%d\n", percent);
    if (percent < 20) {
        Pal.setColor(QPalette::Background, QColor(204, 204, 255));
    } else if (percent < 40) {
        Pal.setColor(QPalette::Background, QColor(153, 153, 255));
    } else if (percent < 60) {
        Pal.setColor(QPalette::Background, QColor(102, 102, 255));
    } else if (percent < 80) {
        Pal.setColor(QPalette::Background, QColor(50, 50, 255));
    } else {
        Pal.setColor(QPalette::Background, QColor(0, 0, 255));
    }
    this->setAutoFillBackground(true);
    this->setPalette(Pal);
}
