#ifndef MEMINFOPARSER_H
#define MEMINFOPARSER_H

#include <QApplication>
#include <QString>
#include <QList>

struct MemInfo {
    long long total;
    long long used;

    MemInfo() {

    }

    MemInfo(long long total, long long used) {
        this->total = total;
        this->used = used;
    }
};

class MemInfoParser
{
private:
    MemInfo* memInfo;
public:
    MemInfoParser(QString str);
    MemInfo* GetMemInfo();
};

#endif // MEMINFOPARSER_H
