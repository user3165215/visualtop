#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "processestab.h"
#include "usagetab.h"
#include "connectdialog.h"
#include "sshconnecter.h"
#include "QMessageBox"

const long MainWindow::UPDATE_INTERVAL = 2 * 1000L;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    con = new SSHConnecter();
    ui->setupUi(this);
    setUpActions();
    setUpTabs(ui->tabWidget);

}

void MainWindow::setUpActions() {
    sshConnectAct = new QAction("&New...",this);
    sshConnectAct->setShortcut(QKeySequence::New);
    sshConnectAct->setStatusTip("New connection");
    connect(sshConnectAct, SIGNAL(triggered()), this, SLOT(sshConnect()));

    QMenu *connection =  ui->menuBar->addMenu("&Connection");
    connection->setStatusTip("Connection");
    connection->addAction(sshConnectAct);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTabs()));
    timer->start(UPDATE_INTERVAL);
}

void MainWindow::sshConnect() {
    ConnectDialog *dialog = new ConnectDialog(con, this);
    int result = dialog->exec();
    if (result == 0) {
        update();
    } else if (result < 0) {
        showError(result);
    }
    delete dialog;
}

void MainWindow::showError(int result) {
    QMessageBox *msg = new QMessageBox(this);
    if (result == -1) {
        msg->setText("libssh2 initialization failed");
    } else if (result == -2) {
        msg->setText("Failed to connect");
    } else if (result == -3 || result == -4) {
        msg->setText("Failure establishing SSH session");
    } else if (result == -5) {
        msg->setText("Authentication by password failed");
    } else {
        msg->setText("Can not connect to ssh");
    }
    msg->exec();
    delete msg;

}

void MainWindow::updateTabs() {
    if (con->isConnected()) {
        processTab->updateData(con);
        usageTab->Update();
    }
}

void MainWindow::setUpTabs(QTabWidget *tabWidget) {
    processTab = new ProcessesTab();
    usageTab = new UsageTab(con, 0);
    tabWidget->addTab(processTab, "Processes");
    tabWidget->addTab(usageTab, "Usage");
}

void MainWindow::disconnect() {
    con->disconnect();
}

MainWindow::~MainWindow()
{
    timer->stop();
    delete timer;
    disconnect();
    delete con;
    delete ui;
}
