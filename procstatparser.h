#ifndef PROCSTATPARSER_H
#define PROCSTATPARSER_H

#include <QApplication>
#include <QString>
#include <QList>
#include <QThread>

class SleeperThread : public QThread
{
public:
    static void msleep(unsigned long msecs)
    {
        QThread::msleep(msecs);
    }
};

struct CpuStat {
    long long user;
    long long nice;
    long long system;
    long long idle;
    long long iowait;
    long long irq;
    long long softirq;

    CpuStat() {

    }

    CpuStat(long long user, long long nice, long long system, long long idle, long long iowait, long long irq, long long softirq) {
        this->user = user;
        this->nice = nice;
        this->system = system;
        this->idle = idle;
        this->iowait = iowait;
        this->irq = irq;
        this->softirq = softirq;
    }
};

class ProcStatParser
{
private:
    QList<CpuStat> cpus;
    CpuStat* summary;
    int coresNumber;
public:
    ProcStatParser(QString str);
    QList<CpuStat> GetCpusStats();
    CpuStat* GetSummary();
};

#endif // PROCSTATPARSER_H
