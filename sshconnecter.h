#ifndef SSHCONNECTER_H
#define SSHCONNECTER_H
#include "libssh2_config.h"
#include <libssh2.h>
#include "libssh2_config.h"
#include <libssh2_sftp.h>
#ifdef HAVE_WINSOCK2_H
# include <winsock2.h>
#endif
#ifdef HAVE_SYS_SOCKET_H
# include <sys/socket.h>
#endif
#ifdef HAVE_NETINET_IN_H
# include <netinet/in.h>
#endif
# ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
# ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif

#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <QString>

class SSHConnecter
{
public:
    SSHConnecter();
    ~SSHConnecter();
    int connect(const char* hostname, int port, const char* username, const char* password);
    int disconnect();
    int exec(const char *command, QString *result);
    bool isConnected();
private:
    bool connected;
    LIBSSH2_SESSION *session;
    LIBSSH2_CHANNEL *channel;
};

#endif // SSHCONNECTER_H
