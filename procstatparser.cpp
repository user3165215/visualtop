#include <QStringList>
#include <stdio.h>
#include <QtDebug>
#include "procstatparser.h"

ProcStatParser::ProcStatParser(QString str)
{
    coresNumber = 0;
    cpus = QList<CpuStat>();
    QStringList lines = str.split("\n");
    for (int i = 0; i < lines.count(); ++i) {
        QString line = lines[i];
        QStringList values = line.split(QRegExp("[\\s]+"));
        if (line.startsWith("cpu ")) {
            summary = new CpuStat(values[1].toLongLong(), values[2].toLongLong(), values[3].toLongLong(),
                    values[4].toLongLong(), values[5].toLongLong(), values[6].toLongLong(), values[7].toLongLong());
        } else if (line.startsWith("cpu")) {
            coresNumber++;
            cpus.append(CpuStat(values[1].toLongLong(), values[2].toLongLong(), values[3].toLongLong(),
                    values[4].toLongLong(), values[5].toLongLong(), values[6].toLongLong(), values[7].toLongLong()));
        }
    }
}

QList<CpuStat> ProcStatParser::GetCpusStats()
{
    return cpus;
}

CpuStat* ProcStatParser::GetSummary()
{
    return summary;
}
