#ifndef LOGICALPROCESSORSWIDGET_H
#define LOGICALPROCESSORSWIDGET_H

#include <QWidget>

namespace Ui {
class LogicalProcessorsWidget;
}

class LogicalProcessorsWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit LogicalProcessorsWidget(int percent, QWidget *parent = 0);
    ~LogicalProcessorsWidget();
    void update(int percent);

protected:
     void paintEvent(QPaintEvent *event);
    
private:
    Ui::LogicalProcessorsWidget *ui;
    int percent;

    void setBackground();
};

#endif // LOGICALPROCESSORSWIDGET_H
