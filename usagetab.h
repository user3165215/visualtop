#ifndef USAGETAB_H
#define USAGETAB_H

#include <QWidget>
#include <QLayout>
#include "procstatparser.h"
#include "sshconnecter.h"
#include "growingplot.h"
#include "meminfoparser.h"
#include "logicalprocessorswidget.h"

namespace Ui {
class UsageTab;
}

class UsageTab : public QWidget
{
    Q_OBJECT
    
public:
    UsageTab(SSHConnecter* con, QWidget *parent = 0);
    ~UsageTab();

    void drawPlot();
    void Update();
private:
    Ui::UsageTab *ui;
    int counter;
    int N;
    CpuStat previous;
    SSHConnecter* con;
    GrowingPlot *cpuPlot;
    GrowingPlot *memPlot;
    QGridLayout *logicalProcessorsLayout;
    QList<LogicalProcessorsWidget*> cpuWidgets;
    QList<CpuStat> previousCpuStats;

    void initLogicalProcessorsLayout();
    CpuStat* GetStat(SSHConnecter *con);
    MemInfo *GetMemInfo(SSHConnecter *con);
    double GetCpuUsage(CpuStat previous, CpuStat current);
    GrowingPlot * GetCpuPlot(SSHConnecter* con);
    GrowingPlot * GetMemoryPlot(SSHConnecter* con);
    void UpdateCpuPlot();
    void UpdateMemoryPlot();
    void createLayout();
    void updateLogicalProcessors();
};

#endif // USAGETAB_H
