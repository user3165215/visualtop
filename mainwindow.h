#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>

#include "sshconnecter.h"
#include "processestab.h"
#include "usagetab.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    static const long UPDATE_INTERVAL;

private slots:
    void sshConnect();
    void updateTabs();
    
private:
    void setUpActions();
    void setUpTabs(QTabWidget *tabWidget);
    void disconnect();
    void showError(int result);
    Ui::MainWindow *ui;
    QAction *sshConnectAct;
    SSHConnecter *con;
    ProcessesTab *processTab;
    UsageTab *usageTab;
    QTimer *timer;
};

#endif // MAINWINDOW_H
