#ifndef PROCESS_H
#define PROCESS_H
#include <QString>
#include <QStringList>
#include <QTableWidget>

using namespace std;


class Process
{
public:
    Process(QStringList columns, QString original);
    void parseValues(QString original);
    static QStringList parseColumns(QString original);
    QStringList getValues();
    QTableWidgetItem* getWidget(int i);
    /**
     *Column names
     */
    static const QString PID;
    static const QString PPID;
    static const QString RUSER;
    static const QString UID;
    static const QString USER;
    static const QString GROUP;
    static const QString TTY;
    static const QString PR;
    static const QString NI;
    static const QString C;
    static const QString CPU;
    static const QString TIME;
    static const QString TIMEPLUS;
    static const QString MEM;
    static const QString VIRT;
    static const QString SWAP;
    static const QString RES;
    static const QString CODE;
    static const QString DATA;
    static const QString SHR;
    static const QString NFLT;
    static const QString NDRT;
    static const QString S;
    static const QString COMMAND;
    static const QString WCHAN;
    static const QString FLAGS;
private:
    void setColumn(int pos, QString value);

    /**
     *Possible columns
     */
    /**
     * @brief pid The task's unique process ID, which periodically wraps, though never restarting at zero.
     */
    int pid;

    /**
     * @brief ppid The process ID of a task's parent.
     */
    int ppid;

    /**
     * @brief ruser The real user name of the task's owner.
     */
    QString ruser;

    /**
     * @brief uid The effective user ID of the task's owner.
     */
    QString uid;

    /**
     * @brief user The effective user name of the task's owner.
     */
    QString user;

    /**
     * @brief group The effective group name of the task's owner.
     */
    QString group;

    /**
     * @brief tty The name of the controlling terminal. This is usually the device (serial port, pty, etc.)
     * from which the process was started, and which it uses for input or output.
     * However, a task need not be associated with a terminal, in which case you'll see '?' displayed.
     */
    QString tty;

    /**
     * @brief pr The priority of the task.
     */
    int pr;

    /**
     * @brief ni The nice value of the task.
     * A negative nice value means higher priority, whereas a positive nice value means lower priority.
     * Zero in this field simply means priority will not be adjusted in determining a task's dispatchability.
     */
    int ni;

    /**
     * @brief c A number representing the last used processor. In a true SMP environment this will likely change frequently since
     * the kernel intentionally uses weak affinity. Also, the very act of running top may break this weak affinity and cause more
     * processes to change CPUs more often (because of the extra demand for cpu time).
     */
    int c;

    /**
     * @brief cpu The task's share of the elapsed CPU time since the last screen update,
     * expressed as a percentage of total CPU time. In a true SMP environment,
     * if 'Irix mode' is Off, top will operate in 'Solaris mode' where a task's
     * cpu usage will be divided by the total number of CPUs.
     * You toggle 'Irix/Solaris' modes with the 'I' interactive command.
     */
    double cpu;

    /**
     * @brief time Total CPU time the task has used since it started.
     * When 'Cumulative mode' is On, each process is listed with the cpu time that it and its dead children has used.
     * You toggle 'Cumulative mode' with 'S', which is a command-line option and an interactive command.
     * See the 'S' interactive command for additional information regarding this mode.
     */
    QString time;

    /**
     * @brief timeplus The same as 'TIME', but reflecting more granularity through hundredths of a second.
     */
    long timeplus;

    /**
     * @brief mem A task's currently used share of available physical memory.
     */
    double mem;

    /**
     * @brief virt The total amount of virtual memory used by the task.
     * It includes all code, data and shared libraries plus pages that have been swapped out.
     * VIRT = SWAP + RES.
     */
    int virt;

    /**
     * @brief swap The swapped out portion of a task's total virtual memory image.
     */
    int swap;

    /**
     * @brief res The non-swapped physical memory a task has used.
     * RES = CODE + DATA.
     */
    int res;

    /**
     * @brief code The amount of physical memory devoted to executable code,
     * also known as the 'text resident set' size or TRS.
     */
    int code;

    /**
     * @brief data The amount of physical memory devoted to other than executable code,
     * also known as the 'data resident set' size or DRS.
     */
    int data;

    /**
     * @brief shr The amount of shared memory used by a task.
     * It simply reflects memory that could be potentially shared with other processes.
     */
    int shr;

    /**
     * @brief nflt The number of major page faults that have occurred for a task.
     * A page fault occurs when a process attempts to read from or write to a virtual page
     * that is not currently present in its address space.
     * A major page fault is when disk access is involved in making that page available.
     */

    int nflt;

    /**
     * @brief ndrt The number of pages that have been modified since they were last written to disk.
     * Dirty pages must be written to disk before the corresponding physical memory location can be used
     * for some other virtual page.
     */
    int ndrt;

    /**
     * @brief s The status of the task which can be one of:
     * 'D' = uninterruptible sleep
     * 'R' = running
     * 'S' = sleeping
     * 'T' = traced or stopped
     * 'Z' = zombie
     * Tasks shown as running should be more properly thought of as 'ready to run'
     * --  their task_struct is simply represented on the Linux run-queue.
     * Even without a true SMP machine, you may see numerous tasks in this state depending
     * on top's delay interval and nice value.
     */
    QString s;

    /**
     * @brief command Display the command line used to start a task or the name of the associated program.
     * You toggle between command line and name with 'c', which is both a command-line option and an interactive command.
     * When you've chosen to display command lines, processes without a command line (like kernel threads) will be shown with only the program name in parentheses, as in this example:
     * ( mdrecoveryd )
     * Either form of display is subject to potential truncation if it's too long to fit in this field's current width. That width depends upon other fields selected, their order and the current screen width.
     * Note: The 'Command' field/column is unique, in that it is not fixed-width.
     * When displayed, this column will be allocated all remaining screen width (up to the maximum 512 characters) to provide for the potential growth of program names into command lines.
     */
    QString command;

    /**
     * @brief wchan Depending on the availability of the kernel link map ('System.map'),
     * this field will show the name or the address of the kernel function in which the task is currently sleeping.
     * Running tasks will display a dash ('-') in this column.
     * Note: By displaying this field, top's own working set will be increased by over 700Kb. Your only means of reducing that overhead will be to stop and restart top.
     */
    QString wchan;

    /**
     * @brief flags This column represents the task's current scheduling flags which are expressed in hexadecimal notation and with zeros suppressed.
     * These flags are officially documented in <linux/sched.h>. Less formal documentation can also be found on the 'Fields select' and 'Order fields' screens.
     */
    QString flags;


    QStringList columns;
    QStringList values;
};

#endif // PROCESS_H
