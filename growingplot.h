#ifndef CPUPLOT_H
#define CPUPLOT_H

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_array.h>
#include <stdlib.h>

class GrowingPlot : public QwtPlot
{
    Q_OBJECT
private:
    QwtArray<double> x;
    QwtArray<double> y;
    int size;
    int current_size;
    QwtPlotCurve *curve;
public:
    explicit GrowingPlot(QWidget *parent = 0);
    GrowingPlot(int n);
    void Append(double x, double y);
signals:
    
public slots:
    
};

#endif // CPUPLOT_H
