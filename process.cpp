#include "process.h"
#include <stdio.h>

const QString Process::PID = "PID";
const QString Process::PPID = "PPID";
const QString Process::RUSER = "RUSER";
const QString Process::UID = "UID";
const QString Process::USER = "USER";
const QString Process::GROUP = "GROUP";
const QString Process::TTY = "TTY";
const QString Process::PR = "PRI";
const QString Process::NI = "NI";
const QString Process::C = "#C";
const QString Process::CPU = "%CPU";
const QString Process::TIME = "TIME";
const QString Process::TIMEPLUS = "TIME+";
const QString Process::MEM = "%MEM";
const QString Process::VIRT = "VIRT";
const QString Process::SWAP = "SWAP";
const QString Process::RES = "RES";
const QString Process::CODE = "CODE";
const QString Process::DATA = "DATA";
const QString Process::SHR = "SHR";
const QString Process::NFLT = "nFLT";
const QString Process::NDRT = "nDRT";
const QString Process::S = "S";
const QString Process::COMMAND = "COMMAND";
const QString Process::WCHAN = "WCHAN";
const QString Process::FLAGS = "Flags";

Process::Process(QStringList columns, QString original)
{
    this->columns = columns;
    parseValues(original);
}

QStringList Process::getValues() {
    return values;
}

QStringList Process::parseColumns(QString original) {
    return original.trimmed().simplified().split(QRegExp("[\\s]+"));
}

void Process::parseValues(QString original)
{
    values = original.trimmed().simplified().split(QRegExp("[\\s]+"), QString::SkipEmptyParts);
    while (values.size() > columns.size()) {
        values[values.size() - 2].append(values.at(values.size() - 1));
        values.pop_back();
    }
    for (int i = 0; i < values.size(); ++i) {
        setColumn(i, values.at(i));
    }
}

void Process::setColumn(int pos, QString value) {
    QString columnName = this->columns.at(pos);
    if (PID.localeAwareCompare(columnName) == 0) {
        pid = value.toInt();
    } else if (PPID.localeAwareCompare(columnName) == 0) {
        ppid = value.toInt();
    } else if (RUSER.localeAwareCompare(columnName) == 0) {
        ruser = value;
    } else if (UID.localeAwareCompare(columnName) == 0) {
        uid = value.toInt();
    } else if (USER.localeAwareCompare(columnName) == 0) {
        user = value;
    }  else if (GROUP.localeAwareCompare(columnName) == 0) {
        group = value;
    } else if (TTY.localeAwareCompare(columnName) == 0) {
        tty = value;
    } else if (PR.localeAwareCompare(columnName) == 0) {
        pr = value.toInt();
    } else if (NI.localeAwareCompare(columnName) == 0) {
        ni = value.toInt();
    } else if (C.localeAwareCompare(columnName) == 0) {
        c = value.toInt();
    } else if (CPU.localeAwareCompare(columnName) == 0) {
        cpu = value.trimmed().toDouble();
    } else if (TIME.localeAwareCompare(columnName) == 0) {
        time = value;
    } else if (TIMEPLUS.localeAwareCompare(columnName) == 0) {
        timeplus = value.toLong();
    } else if (MEM.localeAwareCompare(columnName) == 0) {
        printf("%d\n",value.trimmed().toDouble());
        mem = value.toDouble();
    } else if (VIRT.localeAwareCompare(columnName) == 0) {
        virt = value.toInt();
    } else if (SWAP.localeAwareCompare(columnName) == 0) {
        swap = value.toInt();
    } else if (RES.localeAwareCompare(columnName) == 0) {
        res = value.toInt();
    } else if (CODE.localeAwareCompare(columnName) == 0) {
        code = value.toInt();
    } else if (DATA.localeAwareCompare(columnName) == 0) {
        data = value.toInt();
    } else if (SHR.localeAwareCompare(columnName) == 0) {
        shr = value.toInt();
    } else if (NFLT.localeAwareCompare(columnName) == 0) {
        nflt = value.toInt();
    } else if (NDRT.localeAwareCompare(columnName) == 0) {
        ndrt = value.toInt();
    } else if (S.localeAwareCompare(columnName) == 0) {
        s = value;
    } else if (COMMAND.localeAwareCompare(columnName) == 0) {
        command = value;
    } else if (WCHAN.localeAwareCompare(columnName) == 0) {
        wchan = value;
    } else if (FLAGS.localeAwareCompare(columnName) == 0) {
        flags = value;
    }
}

QTableWidgetItem* Process::getWidget(int i) {
    QTableWidgetItem *item = new QTableWidgetItem();
    QString columnName = columns.at(i);
    if (CPU.localeAwareCompare(columnName) == 0) {
        item->setData(Qt::DisplayRole, cpu);
    } else if (MEM.localeAwareCompare(columnName) == 0) {
        item->setData(Qt::DisplayRole, mem);
    } else {
        item->setText(values.at(i));
    }
    return item;
}
