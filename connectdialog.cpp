#include "connectdialog.h"
#include "ui_connectdialog.h"
#include <QMessageBox>
ConnectDialog::ConnectDialog(SSHConnecter *con, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectDialog)
{
    this->con = con;
    ui->setupUi(this);
    connect(ui->connectBtn, SIGNAL(clicked()), this, SLOT(sshConnect()));
    connect(ui->cancelBtn, SIGNAL(clicked()), this, SLOT(cancel()));

}

void ConnectDialog::cancel() {
    done(1);
}

void ConnectDialog::sshConnect() {
    QString host = ui->host->text();
    int port = ui->port->value();
    QString user = ui->user->text();
    QString password = ui->password->text();
    int result = con->connect(host.toAscii().constData(), port,
                      user.toAscii().constData(), password.toAscii().constData());
    if (result < 0) {
        showError(result);
    } else {
        done(0);
    }
}
void ConnectDialog::showError(int result) {
    QMessageBox *msg = new QMessageBox(this);
    if (result == -1) {
        msg->setText("libssh2 initialization failed");
    } else if (result == -2) {
        msg->setText("Failed to connect");
    } else if (result == -3 || result == -4) {
        msg->setText("Failure establishing SSH session");
    } else if (result == -5) {
        msg->setText("Authentication by password failed");
    } else {
        msg->setText("Can not connect to ssh");
    }
    msg->exec();
    delete msg;

}


ConnectDialog::~ConnectDialog()
{
    delete ui;
}
