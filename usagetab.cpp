#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include "usagetab.h"
#include "ui_usagetab.h"

GrowingPlot * UsageTab::GetCpuPlot(SSHConnecter* con)
{
    GrowingPlot *cpuPlot = new GrowingPlot(N);
    cpuPlot->setTitle("Cpu usage");
    cpuPlot->setAxisScale(QwtPlot::xBottom, 1, N);
    cpuPlot->setAxisScale(QwtPlot::yLeft, 0, 100);
    cpuPlot->enableAxis(QwtPlot::xBottom, false);

    return cpuPlot;
}

GrowingPlot * UsageTab::GetMemoryPlot(SSHConnecter* con)
{
    GrowingPlot *memPlot = new GrowingPlot(N);
    memPlot->setTitle("Memory usage");
    memPlot->setAxisScale(QwtPlot::xBottom, 1, N);
    memPlot->setAxisScale(QwtPlot::yLeft, 0, 100);
    memPlot->enableAxis(QwtPlot::xBottom, false);

    return memPlot;
}

void UsageTab::createLayout()
{
    QVBoxLayout *mainLayout = new QVBoxLayout;

    QHBoxLayout *plotsLayout = new QHBoxLayout;
    plotsLayout->setSpacing(10);
    plotsLayout->addWidget(cpuPlot);
    plotsLayout->addWidget(memPlot);
    logicalProcessorsLayout = new QGridLayout;

    mainLayout->addLayout(plotsLayout);
    mainLayout->addLayout(logicalProcessorsLayout);

    setLayout(mainLayout);
}

void UsageTab::initLogicalProcessorsLayout()
{
    QString result("");
    con->exec("cat /proc/stat", &result);
    ProcStatParser parser = ProcStatParser(result);
    QList<CpuStat> cpuStats = parser.GetCpusStats();
    for (int i = 0; i < cpuStats.count(); ++i) {
        LogicalProcessorsWidget* widget = new LogicalProcessorsWidget(0);
        logicalProcessorsLayout->addWidget(widget, i / 4, i % 4);
        cpuWidgets.append(widget);
    }
}

UsageTab::UsageTab(SSHConnecter* con, QWidget *parent) :
    QWidget(parent)
{
    counter = 0;
    N = 10;
    this->con = con;
    cpuPlot = GetCpuPlot(con);
    memPlot = GetMemoryPlot(con);

    createLayout();
    showMaximized();
}

UsageTab::~UsageTab()
{
    delete ui;
}

CpuStat *UsageTab::GetStat(SSHConnecter *con)
{
    QString result("");
    con->exec("cat /proc/stat", &result);
    ProcStatParser parser = ProcStatParser(result);
    return parser.GetSummary();
}

MemInfo *UsageTab::GetMemInfo(SSHConnecter *con)
{
    QString result("");
    con->exec("free", &result);
    MemInfoParser parser = MemInfoParser(result);
    return parser.GetMemInfo();
}

double UsageTab::GetCpuUsage(CpuStat previous, CpuStat current)
{
    long long user = current.user - previous.user;
    long long nice = current.nice - previous.nice;
    long long system = current.system - previous.system;
    long long idle = current.idle - previous.idle;
    printf("idle: %lld\n", idle);

    long long total = user + nice + system + idle;
    printf("total: %lld\n", total);

    double cpu_u = (double) user / total;
    double cpu_s = (double) system / total;
    double cpu_n = (double) nice / total;

    double cpu = cpu_u + cpu_s + cpu_n;
    printf("cpu: %f\n", cpu);
    return cpu * 100.0;
}


void UsageTab::updateLogicalProcessors()
{
    QList<CpuStat> cpuStats;
    if (cpuWidgets.count() == 0) {
        initLogicalProcessorsLayout();
        QString result("");
        con->exec("cat /proc/stat", &result);
        ProcStatParser parser = ProcStatParser(result);
        cpuStats = parser.GetCpusStats();
    } else {
        QString result("");
        con->exec("cat /proc/stat", &result);
        ProcStatParser parser = ProcStatParser(result);
        cpuStats = parser.GetCpusStats();
        for (int i = 0; i < cpuWidgets.count(); ++i) {
            printf("GetCpuUsage from logical:\n");
            cpuWidgets[i]->update((int)GetCpuUsage(previousCpuStats[i], cpuStats[i]));
        }
    }
    previousCpuStats = cpuStats;
}

void UsageTab::Update()
{
    counter++;
    if (counter > N) {
        cpuPlot->setAxisScale(QwtPlot::xBottom, counter - N + 1, counter);
        memPlot->setAxisScale(QwtPlot::xBottom, counter - N + 1, counter);
    }
    updateLogicalProcessors();
    UpdateCpuPlot();
    UpdateMemoryPlot();
    repaint();
}

void UsageTab::UpdateCpuPlot() {
    CpuStat* summary = GetStat(con);
    cpuPlot->Append(counter, GetCpuUsage(previous, *summary));
    previous = *summary;
    //printf("cpu: %f\n", GetCpuUsage(previous, *summary));
}

void UsageTab::UpdateMemoryPlot() {
    MemInfo* summary = GetMemInfo(con);
    memPlot->Append(counter, (double) summary->used * 100.0 / summary->total);
    //printf("mem: %f\n", (double) stats[i].used * 100.0 / stats[i].total);
}

